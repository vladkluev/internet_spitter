import struct

MCAST_GRP = '224.1.1.1'
MCAST_PORT = 5007
BLOCK_SIZE = 512
metadata_format = "s"
FEC_K = 4
FEC_M = 8
