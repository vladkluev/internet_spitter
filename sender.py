#!/usr/bin/env python
from __future__ import print_function
from __future__ import absolute_import
#from __future__ import unicode_literals
from __future__ import division

import argparse
import common
import hashlib
import json
import math
import os
import socket
import struct
import sys
import time
import zlib

from common import FEC_K, FEC_M
from zfec.easyfec import Encoder
from zfec.easyfec import Decoder

def get_f_size(f):
    """
        returns size of file in bytes
    """
    statinfo = os.stat(f)
    return statinfo.st_size 

def send_metadata(f, size, name):
    msg = json.dumps({"size":size, "blocksize":common.BLOCK_SIZE,
     "checksum":hashlib.md5(f.read()).hexdigest(), "filename":name, 'm':FEC_M, 'k':FEC_K})
    send_packet(msg.encode('UTF-8'))
    f.seek(0)

def send_file(f_name):
    """
        generates packets to send over UDP
    """
    k = FEC_K
    m = FEC_M
    e = Encoder(k, m)
    d = Decoder(k, m)
    size = get_f_size(f_name)
    blocks_read = 0
    seq = 0
    f = open(f_name, 'rb')
    send_metadata(f, size, f_name)
    n_blocks = math.ceil(size / common.BLOCK_SIZE / k)
    #n_blocks = math.ceil(size / common.BLOCK_SIZE)
    print("blocks: ", n_blocks)
    raw_input("continue?")
    while blocks_read < n_blocks:
        print("blocks_read: %s file size: %s" % (blocks_read, size))
        sequence = f.read(common.BLOCK_SIZE*k)
        blocks = e.encode(sequence)
        blocks_read += 1
        block_n = 0
        for block in blocks:
            #print(sys.getsizeof(block))
            assert len(block) <= 1024   
            msg = struct.pack(b'>II', seq, block_n) + block
            checksum = zlib.crc32(msg)
            msg += struct.pack(b'>I', checksum & 0xffffffff)
            send_packet(msg)
            block_n += 1
        seq += 1
    send_metadata(f, size, f_name)

def send_packet(msg):
    print("UDP target IP:", common.MCAST_GRP)
    print("UDP target port:", common.MCAST_PORT)
    print("message:", msg)
   # input("continue?")
    time.sleep(0.1)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    sock.sendto(msg, (common.MCAST_GRP, common.MCAST_PORT))

def main():
    parser = argparse.ArgumentParser(description='Process the flags')
    parser.add_argument('--input', type=str,
                   help='where are the files you want to serve?')
    args = parser.parse_args()
    send_file(args.input)

if __name__ == "__main__":
    main()


