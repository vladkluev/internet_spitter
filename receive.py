#!/usr/bin/env python2.7
from __future__ import print_function
from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import division

import json
import math
import socket
import struct
import sys
import zlib
import copy
from collections import defaultdict

from zfec.easyfec import Encoder
from zfec.easyfec import Decoder
from common import MCAST_GRP, MCAST_PORT

class CRCError(Exception):
    pass


class Block(object):
    def __init__(self, k, m):
        self.k = k
        self.m = m
        self.chunks = [None] * m
        self.seen_chunks = set()
        self.num_chunks_received = 0

    def add_chunk(self, i, payload):
        self.seen_chunks.add(i)
        if self.chunks is not None and self.chunks[i] is None:
            self.chunks[i] = payload
            self.num_chunks_received += 1

    def try_decode(self):
        if self.chunks is not None and self.num_chunks_received >= self.k:
            chunk_indices, received_chunks = zip(
                *((i, c) for i, c in enumerate(self.chunks) if c is not None))
            self.chunks = None
            return Decoder(self.k, self.m).decode(received_chunks,
                                                  chunk_indices, 0)

        return None

    @property
    def missed_chunks(self):
        return self.m - len(self.seen_chunks)


class Listener(object):
    def __init__(self):
        sock = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM,
            socket.IPPROTO_UDP)

        try:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except AttributeError:
            pass

        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)

        sock.bind(('', MCAST_PORT))
        mreq = struct.pack(b"4sl", socket.inet_aton(MCAST_GRP),
                           socket.INADDR_ANY)
        sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        self.sock = sock

    def decode_packet(self, data):
        seq, blk_n = struct.unpack(b'>II', data[:8])
        chk, = struct.unpack(b'>I', data[-4:])
        if zlib.crc32(data[:-4]) & 0xffffffff != chk:
            raise CRCError

        return seq, blk_n, data[8:-4]

    def receive_packet(self):
        data, addr = self.sock.recvfrom(65536)
        return data

    def receive_file(self):
        meta = json.loads(self.receive_packet().decode('UTF-8'))
        bs = meta['blocksize']
        size = meta['size']
        k = meta['k']
        m = meta['m']
        num_seqs = int(math.ceil(size / bs / k))
        num_packets = num_seqs * m
        rec_blocks = defaultdict(lambda: Block(k, m))
        rec_seqs = set()
        CRC_count = 0

        fn = 'rec_' + meta['filename']
        print('Receiving {!r} with a total size of {} and a block size of {}'.format(
            fn, size, bs))

        with open(fn, 'wb') as fp:
            try:
                self.sock.settimeout(10)
                while len(rec_seqs) < num_seqs:
                    data = self.receive_packet()
                    try:
                        seq_n, blk_n, payload = self.decode_packet(data)
                    except CRCError:
                        CRC_count+=1
                        print("CRCError")
                        continue

                    print("Got block {0} of sequence {1}".format(blk_n,
                                                                 seq_n))
                    block = rec_blocks[seq_n]
                    block.add_chunk(blk_n, payload)

                    # if we have reached k blocks, we can decode
                    data = block.try_decode()
                    if data is not None:
                        rec_seqs.add(seq_n)
                        print('Received {} of {} blocks'.format(len(rec_seqs),
                                                                num_seqs))
                        fp.seek(seq_n * bs * k)
                        fp.write(data)

            finally:
                self.sock.settimeout(None)
                for i in xrange(num_seqs):
                    if i not in rec_seqs:
                        print('Missing sequence {}'.format(i))
                print("Encountered {} CRC Errors".format(CRC_count))
                dropped_packets = sum(b.missed_chunks for b in rec_blocks.itervalues())
                print('Dropped {} of {} packets'.format(dropped_packets,
                                                        num_packets))


def receive_file():
    print("Group: {!r} Port: {!r}".format(MCAST_GRP, MCAST_PORT))

    listener = Listener()
    listener.receive_file()


def main():
    receive_file()


if __name__ == '__main__':
    main()
