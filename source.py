from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division
from __future__ import absolute_import

import argparse
import hashlib
import os
import sys
import uuid


#parser.add_argument('source_dir', metavar='-s', type=str,,
 #                  help='where are the files you want to serve?')

class ServeFile:
	"""A class that includes the file to broadcast.

		Attributes:
			file: a file object
			uuid: the uuid the maintain uniqueness
			md5: an md5 hash of the file

	"""

	def __init__(self, f_path):
		"""
			creates the class and metadata for the file
			specified in file_path

			Arguments:
				f_path: a path to the file
		"""
		# file to publish
		self.p_file = open(f_path)
		# md5hash the file contents
		self.md5 = hashlib.md5(self.p_file.read()).hexdigest()
		self.uuid = uuid.uuid4() 




def publish(source_dir):


def main():
	parser = argparse.ArgumentParser(description='Process the flags')
	parser.add_argument('source_dir', metavar='-s', type=str,,
                   help='where are the files you want to serve?')
	args = parser.parseargs()
	publish(args.accumulate(args.source_dir))


if __name__ == "__main__":
	main()